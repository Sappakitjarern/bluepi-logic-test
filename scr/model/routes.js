const routes = class Routes {
  constructor (graphs) {
    // Check types and graphs data
    if (!graphs || typeof graphs !== 'object') {
      throw new TypeError(`Expected an object, got ${typeof graphs}`)
    }
    this.graphs = graphs
  }
  // To init the map, define a 'visited' property on the node, which is writable but not enumerable.
  initHashMap = () => {
    const keys = Object.keys(this.graphs)
    keys.forEach((key) => {
      Object.defineProperty(
        this.graphs[key],
        "visited",
        { value: false, enumerable: false, writable: true }
      )
    })
  }

  resetGraphsHashMap = () => {
    const keys = Object.keys(this.graphs)
    keys.forEach((key) => {
      this.graphs[key].visited = false
    })
  }
  
  findNumberOfStops = (node, depth = 0, maxStops = 1, condition) => {
    let routes = 0;
    let count = 0
    const nodes = node.toUpperCase().replace(/\s+/g, '').split('-')
    const start = nodes[0]
    const end = nodes[1]
    this.initHashMap()
    !this.graphs[start]?.visited || this.resetGraphsHashMap()

    if(this.graphs.hasOwnProperty(start)) {
      depth++;
      if (depth > maxStops) { //end of the recursion
        return 0;
      }
      this.graphs[start].visited = true // flag visited data
      const nextNodes = Object.keys(this.graphs[start])
      nextNodes.forEach((keyOfNextNode) => {
        count++
        if (condition === 'max') {
          if (keyOfNextNode === end) {
            routes++
          } else {
            let newNode = `${keyOfNextNode}-${end}`
            routes += this.findNumberOfStops(
              newNode,
              depth,
              maxStops,
              condition
            )//recursion
          }
        } else if (condition === 'count') {
          routes = count
        }
      })
    } else {
      return 'NO SUCH ROUTE'
    }
    this.graphs[start].visited = false // flag visited data
    return routes
  }
  
  lengthOfShortestRoute = (node, currentDistance = 0, minRoute = 0) => {
    const nodes = node.toUpperCase().replace(/\s+/g, '').split('-')
    const start = nodes[0]
    const end = nodes[1]
    minRoute == 0 || this.initHashMap()
    !this.graphs[start]?.visited || this.resetGraphsHashMap()

    if (this.graphs.hasOwnProperty(start)) {
      this.graphs[start].visited = true
      const nextNodes = Object.keys(this.graphs[start])
      nextNodes.forEach((keyOfNextNode) => {
        const newDistance = currentDistance + this.graphs[start][keyOfNextNode]
  
        //If it reach the end of the recursion
        if (keyOfNextNode === end) {
          this.graphs[start].visited = false
          if (newDistance < minRoute || minRoute === 0) minRoute = newDistance
          return minRoute
        } else if (!this.graphs[keyOfNextNode].visited) {
          let newNode = `${keyOfNextNode}-${end}`
          minRoute = this.lengthOfShortestRoute(newNode, newDistance, minRoute) //recursion
        }
      })
    } else {
      return 'NO SUCH ROUTE'
    }
    return minRoute
  }

  numberOfRoutes = (node, currentDistance = 0, maxDistance = 0) => {
    let routes = 0;
    const nodes = node.toUpperCase().replace(/\s+/g, '').split('-')
    const start = nodes[0]
    const end = nodes[1]

    if (this.graphs.hasOwnProperty(start)) {
      const nextNodes = Object.keys(this.graphs[start])
      nextNodes.forEach((keyOfNextNode) => {
        const newDistance = currentDistance + this.graphs[start][keyOfNextNode]
        if (newDistance >= maxDistance) { //end of the recursion
          return 0
        }
        if (keyOfNextNode === end) {
          routes++
        }
        //even though touched the target of the point, can still continue
        let newNode = `${keyOfNextNode}-${end}`
        routes += this.numberOfRoutes(newNode, newDistance, maxDistance)
      })
    } else {
      return 'NO SUCH ROUTE'
    }
    return routes
  }
}

module.exports = routes