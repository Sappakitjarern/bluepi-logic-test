const distance = class Distance {
  constructor (graphs) {
    // Check types and graphs data
    if (!graphs || typeof graphs !== 'object') {
      throw new TypeError(`Expected an object, got ${typeof graphs}`)
    }
    this.graphs = graphs
  }

  calculateDistance = (node) => {
    let distance = 0
    const nodes = node.toUpperCase().replace(/\s+/g, '').split('-')
    for (let i = 0; i < nodes.length - 1; i++) {
      const currentDistance = this.graphs[nodes[i]][nodes[i + 1]]
      if (!currentDistance) {
        return 'NO SUCH ROUTE'
      }
      distance += currentDistance
    }
    return distance
  }
}


module.exports = distance