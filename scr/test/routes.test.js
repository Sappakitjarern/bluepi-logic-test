const routes = require("../model/routes.js")

const train = new routes({
  A: { B: 5, D: 5, E: 7 },
  B: { C: 4 },
  C: { D: 8, E: 2 },
  D: { C: 8, E: 6 },
  E: { B: 3 }
})

describe('Add parameters to constructor.', () => {
  test("Should throw error", () => {
    expect(() => {new routes()}).toThrow();
  })
})

describe('Reset hashmap data.', () => {
  test("Should have data object", () => {
    expect(train).toMatchObject({resetGraphsHashMap: expect.any(Function)})
  })
})

describe('Find trips number (maximum and count) of stop.', () => {
  test('Should the number of trips starting at C and ending at C with a maximum of 3 stops is 2', () => {
    expect(train.findNumberOfStops('C-C', 0, 3, 'max')).toBe(2)
  })
  
  test('Should the number of trips starting at A and ending at C with exactly 4 stops is 3', () => {
    expect(train.findNumberOfStops('A-C', 0, 4, 'count')).toBe(3)
  })
  
  test('Should error message', () => {
    expect(train.findNumberOfStops('@A-C', 0, 4, 'count')).toBe('NO SUCH ROUTE')
  })
})

describe('Find length of the shortest route.', () => {
  test('Should the length of the shortest route (in terms of distance to travel) from A to C is 9', () => {
    expect(train.lengthOfShortestRoute('A-C')).toBe(9)
  })
  
  test('Should the length of the shortest route (in terms of distance to travel) from B to B is 9', () => {
    expect(train.lengthOfShortestRoute('B-B')).toBe(9)
  })
  
  test('Should error message', () => {
    expect(train.lengthOfShortestRoute('@A-C')).toBe('NO SUCH ROUTE')
  })
})

describe('Find the number of different routes with a distance of less than max maximum distance.', () => {
  it('Should the number of different routes from C to C with a distance of less than 30 is 7', () => {
    expect(train.numberOfRoutes('C-C', 0, 30)).toBe(7)
  })

  it('Should error message', () => {
    expect(train.numberOfRoutes('@C-C', 0, 30)).toBe('NO SUCH ROUTE')
  })
})