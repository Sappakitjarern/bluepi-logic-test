const distance = require("../model/distance.js")

const train = new distance({
  A: { B: 5, D: 5, E: 7 },
  B: { C: 4 },
  C: { D: 8, E: 2 },
  D: { C: 8, E: 6 },
  E: { B: 3 }
})

describe('Add parameters to constructor.', () => {
  test("Should throw error", () => {
    expect(() => {new distance()}).toThrow();
  })
})

describe('Calculate Route Distance.', () => {
  // route A-B-C.
  test('Should the distance of the route A-B-C is 9', () => {
    expect(train.calculateDistance('A-B-C')).toBe(9)
  })

  // route A-D..
  test('Should the distance of the route A-D is 5', () => {
    expect(train.calculateDistance('A-D')).toBe(5)
  })

  // route A-B-C.
  test('Should the distance of the route A-D-C is 13', () => {
    expect(train.calculateDistance('A-D-C')).toBe(13)
  })

  // route A-B-C.
  test('Should the distance of the route A-E-B-C-D is 22', () => {
    expect(train.calculateDistance('A-E-B-C-D')).toBe(22)
  })
  
  // route A-B-C.
  test('Should the distance of the route A-E-D NO SUCH ROUTE', () => {
    expect(train.calculateDistance('A-E-D')).toBe('NO SUCH ROUTE')
  })
})