const distance = require('./model/distance.js')
const routes = require('./model/routes.js')

const graphs = () => {
  const graphData = {
    A: { B: 5, D: 5, E: 7 },
    B: { C: 4 },
    C: { D: 8, E: 2 },
    D: { C: 8, E: 6 },
    E: { B: 3 }
  } 
  const train = {
    ...new distance(graphData),
    ...new routes(graphData)
  }

  console.log(`1. The distance of the route A-B-C: ${train.calculateDistance('A-B-C')}`)
  console.log(`2. The distance of the route A-D: ${train.calculateDistance('A-D')}`)
  console.log(`3. The distance of the route A-D-C: ${train.calculateDistance('A-D-C')}`)
  console.log(`4. The distance of the route A-E-B-C-D: ${train.calculateDistance('A-E-B-C-D')}`)
  console.log(`5. The distance of the route A-E-D: ${train.calculateDistance('A-E-D')}`)

  //Find the number of trips starting at C and ending at C with a maximum of 3 stops.
  console.log(
    '6. The number of trips starting at C and ending at C with a maximum of 3 stops:',
    train.findNumberOfStops('C-C', 0, 3, 'max')
  )
  console.log(
    '7. The number of trips starting at A and ending at C with exactly 4 stops:',
    train.findNumberOfStops('A-C', 0, 4, 'count')
  )
  console.log(
    '8. The length of the shortest route (in terms of distance to travel) from A to C:',
    train.lengthOfShortestRoute('A-C'));
  console.log(
    '9. The length of the shortest route (in terms of distance to travel) from B to B:',
    train.lengthOfShortestRoute('B-B'));
  console.log(
    `10. The number of different routes from C to C with a distance of less than 30.
    In the sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC, CEBCEBC, CEBCEBCEBC:`,
    train.numberOfRoutes('C-C', 0, 30));
}

module.exports = graphs()