# __Software Engineer Challenge__

__Challenge Timeline:__ 3 Days from the date that you receive a challenge\
__Note For Candidate__
- For the solution, we request that you use __Python, JavaScript or TypeScript, or Go
(Choose one of these)__
- The application must run
- We recommended using TDD to solve these problems, At least have Test Coverage
for your solution.
- Include unit tests in your solution. We recommend using TDD to solve these
problems.
- Submit production-ready code that is clean and easy to understand.
- We allow you to guess and provide creative solutions to our problems. Sometimes
you will find a grab of requirements that always happen in day-to-day work. Use your
imagination and your problem-solving skill to tackle a problem.

__Rules:__
1. A Solution should be a __<u>command-line application</u>__, We didn’t require any GUI.
2. You may not use any external libraries to solve this problem, but you may use
external libraries or tools for building or testing purposes. Specifically, you may use
unit-testing libraries or build tools available for your chosen language.
3. Please include a brief explanation of your design and assumptions, along with your
code, as well as detailed instructions to run your application.
4. We want our hiring process to be fair, and for everyone to start from the same place.
To enable this, we request that you do not share or publish these problems.

## __Coding Problem__
### __Problem:__ A Tour Route Planner
#### __Background__

The local commuter railroad services a number of towns in Kiwiland. Because of monetary
concerns, all of the tracks are 'one-way.' That is, a route from Kaitaia to Invercargill does not
imply the existence of a route from Invercargill to Kaitaia. In fact, even if both of these routes
do happen to exist, they are distinct and are not necessarily the same distance!

The purpose of this problem is to help the railroad provide its customers with information
about the routes. In particular, you will compute the distance along a certain route, the
number of different routes between two towns, and the shortest route between two towns.

#### __Input:__
A directed graph where a node represents a town and an edge represents a route between
two towns. The weighting of the edge represents the distance between the two towns. A
given route will never appear more than once, and for a given route, the starting and ending
town will not be the same town.

#### __Output:__
For test input 1 through 5, if no such route exists, output 'NO SUCH ROUTE'. Otherwise,
follow the route as given; do not make any extra stops! For example, the first problem
means to start at city A, then travel directly to city B (a distance of 5), then directly to city C
(a distance of 4).

``` js
The distance of the route A-B-C.
The distance of the route A-D.
The distance of the route A-D-C.
The distance of the route A-E-B-C-D.
The distance of the route A-E-D.
The number of trips starting at C and ending at C with a maximum of 3 stops. In the
sample data below, there are two such trips: C-D-C (2 stops). and C-E-B-C (3 stops).
The number of trips starting at A and ending at C with exactly 4 stops. In the sample
data below, there are three such trips: A to C (via B,C,D); A to C (via D,C,D); and A to
C (via D,E,B).
The length of the shortest route (in terms of distance to travel) from A to C.
The length of the shortest route (in terms of distance to travel) from B to B.
The number of different routes from C to C with a distance of less than 30. In the
sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC, CEBCEBC, CEBCEBCEBC.
```

#### __Test Input:__
For the test input, the towns are named using the first few letters of the alphabet from A to D.
A route between two towns (A to B) with a distance of 5 is represented as AB5.\
__Graph: AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7__

``` js
Expected Output:
Output #1: 9
Output #2: 5
Output #3: 13
Output #4: 22
Output #5: NO SUCH ROUTE
Output #6: 2
Output #7: 3
Output #8: 9
Output #9: 9
Output #10: 7
```

## __Build Setup__

```bash
# Install dependencies
$ npm install

# Start for show result
$ npm run start

# Start for show test coverage
$ npm run test